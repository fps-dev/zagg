<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package zagg
 */

get_header();
?>



	<div id="primary" class="content-area relative  ">
		<div class="bg-home" style="background-image: url('<?php the_field('bg_general_tablet','option'); ?>');"></div>
		<div class="bg-home-tablet " style="background-image: url('<?php the_field('bg_general_tablet','option'); ?>');"></div>
		<div class="bg-home-mobile " style="background-image: url('<?php the_field('bg_general_mobile','option'); ?>');"></div>
		 <main id="main" class="site-main flex flex-wrap items-center h-95 z-50 overflow-auto">
			<?php while ( have_posts() ) : the_post(); ?>
				<div class="container px-4 mx-auto py-12">
					
						<div class="grid grid-cols-12 gap-4 items-center relative z-50">
							<div class="col-span-12 md:col-start-2 md:col-span-10 lg:col-span-5 px-0 md:px-8 lg:px-0 text-center lg:text-left">
								<!-- logo -->
								<?php $GETlogo = get_field('logo_site','option'); ?>
								<a class="site-header__logo logo-home" href="<?php echo esc_url(get_bloginfo('url'));?>">
									<img class="custom-logo"src="<?php echo $GETlogo['url']; ?>" alt="<?php echo $GETlogo['alt'];?>" />
								</a>
								<h2 class="title mb-6"><?php the_field('title_coming-soon') ?></h2>
								<pc class="lead dark-3"><?php the_field('description_coming-soon') ?></p>
							</div>
							<div class="col-span-12 md:col-start-2 md:col-span-10 lg:col-start-7 lg:col-span-6">
								<div class="form">
									<?php the_content(); ?>
								</div>
							</div>
						</div>
					
						<div class="list-logos grid grid-cols-12 gap-4 items-center py-6 relative z-50">
								<?php if(get_field('logos')): ?>
									<?php while(has_sub_field('logos')): ?>
										<div class="col-span-4 lg:col-span-2 flex flex-wrap items-center justify-center">
											<div class="item-list ">
												<img src="<?php the_sub_field('logo_coming-soon'); ?>" />
											</div>
										</div>
									<?php endwhile; ?>
								<?php endif; ?>
						</div>
					
						

				</div>
			<?php endwhile; ?>
		 </main><!-- #main -->
	 </div><!-- #primary -->

<?php
get_footer();
