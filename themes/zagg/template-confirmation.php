<?php
/**
 * TEMPLATE NAME:  Confirmation template
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package zagg
 */

get_header();
?>

    <div class="bg-confirmation" style="background-image: url('<?php the_field('bg_general_tablet','option'); ?>');"></div>

	<div id="primary" class="content-area">
		<main id="main" class="site-main flex flex-wrap items-center h-82">
        <?php while ( have_posts() ) : the_post();?>
			<div class="container mx-auto">
				<div class="grid grid-cols-12 gap-4 h-full items-center justify-center">
					<div class="content-confirmation mx-4 col-span-12 md:col-start-2 md:col-span-10 lg:col-start-3 lg:col-span-8 flex flex-wrap items-center justify-center lg:justify-start relative">
						<img class="icon-confirmation" src="<?php the_field('icon_confirmation'); ?>" alt="">
						<h1 class="title mb-4 mt-6 lg:mt-0 justify-center lg:justify-start z-50"><?php the_field('title_confirmation') ?></h1>
						<p class="lead w-full text-center z-50"><?php the_field('description_confirmation') ?></p>
						<?php $link = get_field('link_confirmation');
							if( $link ): 
								$link_url = $link['url'];
								$link_title = $link['title'];
								$link_target = $link['target'] ? $link['target'] : '_self';
								?>
								<a class="btn" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
							<?php endif; ?>
					</div><!-- text -->
				</div>
			</div>
            <?php endwhile; ?>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();