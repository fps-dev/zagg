let gulp = require('gulp'),
    rename = require("gulp-rename"),
    sass = require('gulp-sass'),
    postcss = require('gulp-postcss'),
    cssnano = require('gulp-cssnano'),
    browserSync = require("browser-sync").create();
// Put this after including our dependencies
let paths = {
   styles: {
      src: "assets/sass/**/*.scss", //Css Origin
      dest: "public/css/" //Css Destination
   },
   php: {
      src: "**/*.php", //Watching all php files
   },
   js: {
      src: "assets/js/**/*.js", //Watching all js files
   }
};
// Define tasks after requiring dependencies
function style() {
   return (
      gulp
         .src( paths.styles.src )
         .pipe(postcss([
            require('postcss-import'),
            require('tailwindcss'),
            require('autoprefixer'),
            require('cssnano'),
         ]))
         .pipe(rename("main.min.css"))
         .pipe(sass().on('error', sass.logError))
         .pipe(cssnano())
         .pipe( gulp.dest(paths.styles.dest) )
         .pipe(browserSync.stream())
   );
}
// $ gulp style
exports.style = style;
//Reload
function reload() {
    browserSync.reload();
}
//Watching
function watch(){
   let files = [
     paths.styles.src,
     paths.php.src,
     paths.js.src,
   ];
   browserSync.init({
      proxy: "http://localhost:10053/"
      /*https: {
           key: "/Applications/MAMP/Library/OpenSSL/certs/localhost.key",
           cert: "/Applications/MAMP/Library/OpenSSL/certs/localhost.crt"
      }*/
   });
    gulp.watch(paths.styles.src, style);
    gulp.watch( files, reload);
}
// $ gulp watch
exports.watch = watch
