<?php
/**
 * Enqueue scripts and styles.
 */
function zagg_scripts() {

	wp_enqueue_style( 'zagg-style', get_stylesheet_uri());
	wp_enqueue_style( 'fonts', 'https://fast.fonts.net/cssapi/91550fb1-13d5-4985-bd5a-1e83c53f90e9.css');
	wp_enqueue_style( 'google-font-material-icon', 'https://fonts.googleapis.com/icon?family=Material+Icons');
    wp_enqueue_style( 'main-styles', get_template_directory_uri().'/public/css/main.min.css', null, '1.0.5', false );

	wp_enqueue_script( 'zagg-navigation', get_template_directory_uri() . '/public/js/zagg.js', array('jquery'), '1.0.0', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'zagg_scripts' );