<!--Header-->
<header id="masthead" class="<?php if(is_front_page()){ ?> hidden <?php } ?>">
  

    <!--  main menu -->
    <div class="flex flex-wrap items-center">
      <div class="container mx-auto relative">
        <div class="site-header">

          <div class="grid grid-flow-col grid-cols-12 gap-4  w-full p-4">

                <div class="col-span-12 flex flex-wrap items-center justify-center lg:justify-start">
                    <!-- logo -->
                    <?php $GETlogo = get_field('logo_site','option'); ?>
                    <a class="site-header__logo  " href="<?php echo esc_url(get_bloginfo('url'));?>">
                        <img class="custom-logo"src="<?php echo $GETlogo['url']; ?>" alt="<?php echo $GETlogo['alt'];?>" />
                    </a>
                </div>

          </div>

        </div>
      </div>
    </div>
    <!--  end main menu -->

</header><!-- #masthead -->
