<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package zagg
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<!--Google Tag Manager-->
	<?php the_field('google_tag_header','option'); ?>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<!--Favicon-->
	<link rel="icon" href="<?php the_field('favicon', 'option');?>">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<?php wp_body_open(); ?>

<div id="page" class="site">

	<!--Google Tag Manager-->
	<?php the_field('google_tag','option'); ?>
	<!--/Google Tag Manager-->

	<!-- Site Header  -->
		<?php get_template_part( 'inc/site', 'header' ); ?>
 	<!-- /Site Header  - All pages -->
