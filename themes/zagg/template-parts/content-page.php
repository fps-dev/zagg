<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package zagg
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			
			<div class="flex flex-wrap ">
					<div class="container mx-auto px-4 py-12">
						<!-- title -->
						<section>
							<div class="entry-content flex flex-wrap">
								<?php the_content(); ?>
							</div>
						</section>
						<!-- end title -->
					</div>
			</div>

</article><!-- #post-<?php the_ID(); ?> -->
