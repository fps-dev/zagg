<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package zagg
 */

?>



<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

<div class="container mx-auto px-4 pt-16">
	<div class="grid grid-cols-12 gap-4 items-center">
		<div class="col-span-12  lg:col-start-3 lg:col-span-8 ">
				<header class="entry-header">
					<?php
					if ( is_singular() ) :
						the_title( '<h1 class="entry-title">', '</h1>' );
					else :
						the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
					endif;
					?>
				</header><!-- .entry-header -->
				<div class="entry-content flex flex-wrap">
					<?php the_content(); ?>
				</div>
		</div>
	</div>
</div>






</article><!-- #post-<?php the_ID(); ?> -->
