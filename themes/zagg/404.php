<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package zagg
 */

get_header();
?>
	<div class="bg-404" style="background-image: url('<?php the_field('background_404','option'); ?>');"></div>
	<div id="primary" class="content-area">
		<main id="main" class="site-main">
		
			<div class="container mx-auto">
				<div class="grid grid-cols-12 gap-4 h-83 items-center py-10">
					<div class="content-404 px-4 col-span-12 lg:col-span-6 flex flex-wrap items-center justify-center lg:justify-start relative">
						<span class="num-404">404</span>
						<h1 class="title mb-4 mt-12 lg:mt-0 justify-center lg:justify-start z-50"><?php the_field('title_404','option') ?></h1>
						<p class="lead primary-dark-2 w-full text-center lg:text-left z-50"><?php the_field('description_404','option') ?></p>
						<?php $link = get_field('link_404','option');
							if( $link ): 
								$link_url = $link['url'];
								$link_title = $link['title'];
								$link_target = $link['target'] ? $link['target'] : '_self';
								?>
								<a class="btn" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
							<?php endif; ?>
					</div><!-- text -->
				</div>
			</div>
		
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();

