(function ($) {

    $(document).ready(function() {

        $('input').each(function() {
    
          $(this).on('focus', function() {
            $(this).parent().parent('.gfield').addClass('active');
          });
    
          $(this).on('blur', function() {
            if ($(this).val().length == 0) {
              $(this).parent().parent('.gfield').removeClass('active');
            }
          });
          
          if ($(this).val() != '') $(this).parent('.css').addClass('active');
    
        });
      $('textarea').each(function() {
    
          $(this).on('focus', function() {
            $(this).parent().parent('.gfield').addClass('active');
          });
    
          $(this).on('blur', function() {
            if ($(this).val().length == 0) {
              $(this).parent().parent('.gfield').removeClass('active');
            }
          });
          
          if ($(this).val() != '') $(this).parent('.css').addClass('active');
    
        });
      });
        
    
    }) ( jQuery );

