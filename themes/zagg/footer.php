<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package zagg
 */

?>

<div class="footer">
		<div class="container mx-auto">
					
			<div class="footer-police">
				<div class="grid grid-cols-12 gap-4 items-center">
					<div class="col-span-12 md:col-span-7">
						<div class="footer-police__submenu">
							<?php
								if( have_rows('privacy_policy' , 'option') ):
									while ( have_rows('privacy_policy' , 'option') ) : the_row();
											
										$link = get_sub_field('item_privacy');
										if( $link ): 
											$link_url = $link['url'];
											$link_title = $link['title'];
											$link_target = $link['target'] ? $link['target'] : '_self';
											?>
											<a class="button" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
										<?php endif; 

									endwhile;
								endif;
							?>
						</div>
					</div>
					<div class="col-span-12 md:col-span-5">
						<div class="footer-police__copy">
							<p><?php the_field('copyright' , 'option'); ?></p>
						</div>
					</div>
				</div>
			</div><!-- footer-police -->

		</div><!-- container -->
	</div> <!-- footer -->


</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
